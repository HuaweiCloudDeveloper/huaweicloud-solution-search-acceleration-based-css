[TOC]

**解决方案介绍**
===============
该方案基于华为云CSS、RDS以及DRS的服务组合，快速构建一个低门槛、低成本的SQL加速方案，零代码实现数据的实时同步，帮助企业应对搜索流量高峰、提升多维分析性能。

适用于电商、物流等行业的运营部门对商品订单、物流信息等进行多维度查询和分析。

解决方案实践详情页面：https://www.huaweicloud.com/solution/implementations/search-acceleration-based-css.html


**架构图**
---------------
![方案架构](./document/search-acceleration-based-css-new-vpc.png)

**架构描述**
---------------
该解决方案会部署如下资源：

- 创建云数据库服务RDS，用于存储用户业务数据。
- 通过数据复制服务DRS，用于将RDS数据实时同步到CSS。
- 云搜索服务CSS是一个完全兼容开源Elasticsearch且完全托管的在线分布式搜索服务，用于提供高并发、低时延的查询分析能力。

**组织结构**
---------------

``` lua
huaweicloud-solution-search-acceleration-based-CSS
├── search-acceleration-based-css-new-vpc.tf.json -- 资源编排模板
```
**开始使用**
---------------
**安全组规则修改（可选）**

安全组实际是网络流量访问策略，包括网络流量入方向规则和出方向规则，通过这些规则为安全组内具有相同保护需求并且相互信任的云服务器、云容器、云数据库等实例提供安全保护。
如果您的实例关联的安全组策略无法满足使用需求，比如需要添加、修改、删除某个TCP端口，请参考以下内容进行修改。
- 添加安全组规则：根据业务使用需求需要新开放某个TCP端口，请参考[添加安全组规则](https://support.huaweicloud.com/usermanual-vpc/zh-cn_topic_0030969470.html)添加入方向规则，打开指定的TCP端口。
- 修改安全组规则：安全组规则设置不当会造成严重的安全隐患。您可以参考[修改安全组规则](https://support.huaweicloud.com/usermanual-vpc/vpc_SecurityGroup_0005.html)，来修改安全组中不合理的规则，保证云服务器等实例的网络安全。
- 删除安全组规则：当安全组规则入方向、出方向源地址/目的地址有变化时，或者不需要开放某个端口时，您可以参考[删除安全组规则](https://support.huaweicloud.com/usermanual-vpc/vpc_SecurityGroup_0006.html)进行安全组规则删除

**同步数据**

部署完成后，会自动创建MySQL数据库及CSS数据库集群，本章节主要指导用户配置DRS，完成数据同步。

1.进入[DRS数据复制服务](https://console.huaweicloud.com/drs/?region=cn-north-4#/drs/migrate/list)控制台，选择“实时同步管理”，单击“创建同步任务”。

图1 DRS数据复制服务控制台

![DRS数据复制服务控制台](./document/readme-image-001.png)

2.配置同步实例信息，选择“出云”方向，源数据库引擎选择“MySQL”，目标数据库引擎选择“CSS/ES”，网络类型选择“vpc网络”，选择对应的mysql实例名称，选择同步实例所在的子网。单击“下一步”。

图2 同步实例信息

![同步实例信息](./document/readme-image-002.png
)

3.等待同步实例创建成功后，配置源库及目标库，输入该方案自动部署的mysql数据库用户（root）名密码（部署方案时设置的密码），单击“测试连接”；输入该方案自动部署的数据库CSS的连接IP（获取方式如图4获取目标库CSS的连接IP）、用户名（admin）和密码（部署方案时设置的密码）、选择加密证书（获取方式如图5下载安全证书），单击“测试连接”。然后单击“下一步”。  


图3 配置源库及目标库

![配置源库及目标库](./document/readme-image-003.png)

图4 获取目标库CSS的连接IP

![获取目标库CSS的连接IP](./document/readme-image-004.png)

图5 下载安全证书

![下载安全证书](./document/readme-image-005.png)

4.设置同步，勾选同步对象，并单击右箭头的添加按钮，单击“下一步”。

图6  设置同步

![设置同步](./document/readme-image-006.png)

5.数据加工，根据需要选择数据加工的方式，该方案实例不需要数据加工，单击“下一步”。

6.预检查，等待完成预检查，单击“下一步”。

图7 预检查

![预检查](./document/readme-image-007.png)

7.启动任务，选择“立即启动”，单击“启动任务”，等待同步任务完成全量同步，进入增量同步状态。

图8 任务确认

![任务确认](./document/readme-image-008.png)

**查询测试**

本章节主要指导用户如何使用CSS进行数据查询。

1.进入华为云[CSS服务控制台](https://console.huaweicloud.com/elasticsearch/?region=cn-north-4#/es/management/dashboard)，选择该方案创建的CSS服务的kibana，进入kibana界面。

图9 CSS服务

![CSS服务](./document/readme-image-009.png)

2.输入用户名（admin）密码（部署解决方案时设置的密码），登入kibana。

图10 kibana登录

![kibana登录](./document/readme-image-010.png)

3.单击左上角的标志，选择“DevTools”。

图11 选择DevTools

![选择DevTools](./document/readme-image-011.png)

4.在Dev Tools的调试界面，输入以下查询语句进行调试，并单击语句后的运行键运行：#查询商户表中所有成都的商户

GET t_user_store_info/_search?q=city:"成都"

图12 查询结果

![选择DevTools](./document/readme-image-012.png)


查询结果显示成都商户，表示查询成功。


